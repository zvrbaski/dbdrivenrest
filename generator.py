import json
import tornado.web
import tornado.ioloop
import traceback
from sqlalchemy.orm.attributes import InstrumentedAttribute
from tornado_json.requesthandlers import APIHandler
from tornado_cors import CorsMixin
from sqlalchemy import create_engine
from sqlalchemy import inspect
from sqlalchemy.orm import Session
from sqlalchemy.ext.automap import automap_base, ForeignKeyConstraint


def HandlerFactory(name, data_class, classes, engine):
    data_class = data_class
    foreign_keys = {
        x: classes[str(x.column.table.name)] for x in
        inspect(data_class).mapped_table.foreign_keys
        }
    primary_keys = inspect(data_class).primary_key[0].name

    def respond(self, code, data):

        if isinstance(data, list):
            response_data = list()
            for row in data:
                if isinstance(row, dict) or isinstance(data, str):
                    response_data.append(row)
                else:
                    response_data.append(
                        {x: getattr(row, x) for x in row.__dict__ if
                         not x.startswith('_')
                         })
        elif isinstance(data, dict) or isinstance(data, str):
            response_data = data
        elif data:
            response_data = {x: getattr(data, x) for x in data.__dict__ if
                             not x.startswith('_')}
        else:
            response_data = data
        response_content = response_data
        self.set_status(code)
        if data:
            self.write(json.dumps(response_content).encode('utf-8'))

    def get(self, id=None):
        session = Session(self.engine)
        try:
            if id:
                result = session.query(data_class).filter_by(
                    **{primary_keys[0].name: id}).all()
                if not result:
                    self.respond(404, None)
                else:
                    if len(result) == 1:
                        self.respond(200, result[0])
                    else:
                        self.respond(200, result)
            else:
                result = session.query(data_class).all()
                self.respond(200, result)
        except KeyError as key_err:
            print("Entity not found")
            traceback.print_exc()
            self.respond(404, str(key_err))
        except Exception as ex:
            print("Unexpected error during GET", ex)
            traceback.print_exc()
            self.respond(500, "Unexpected error during GET method")
        finally:
            session.close()

    def delete(self, id):
        session = Session(self.engine)
        try:
            result = session.query(data_class).filter_by(
                **{primary_keys[0].name: id}).all()
            if len(result) == 1:
                session.delete(result[0])
                session.commit()
                self.respond(204, None)
            elif len(result) == 0:
                self.respond(404, None)
            else:
                self.respond(409, "Multiple instances found")
        except KeyError as key_err:
            print("Entity not found")
            traceback.print_exc()
            self.respond(404, str(key_err))
        except Exception as ex:
            print("Unexpected error during DELETE", ex)
            traceback.print_exc()
            self.respond(500, "Unexpected error during DELETE method")
        finally:
            if session:
                session.close()

    def post(self):
        session = Session(self.engine, autoflush=True)
        try:
            data = json.loads(self.request.body.decode("utf-8"))
            for key in foreign_keys:
                if key in data:
                    related = session.query(foreign_keys[key]).filter_by(
                        id=data[key]).one()
                    data[key] = related
            instance = data_class(**data)
            session.add(instance)
            session.flush()
            session.refresh(instance)
            session.expunge(instance)
            session.commit()
            self.respond(201, instance)
        except KeyError as key_err:
            print("Entity not found")
            traceback.print_exc()
            self.respond(404, str(key_err))
        except Exception as ex:
            print("Unexpected error during POST", ex)
            traceback.print_exc()
            self.respond(500, "Unexpected error during POST method")
        finally:
            session.close()

    def patch(self, id):
        session = Session(self.engine)
        try:
            data = json.loads(self.request.body.decode("utf-8"))
            if data.get('id', None) != id:
                self.respond(500, "Inconsistent ids provided.")
                return
            result = session.query(data_class).filter_by(
                **{primary_keys[0].name: id}).all()
            if len(result) == 1:
                for name, value in data.items():
                    setattr(result[0], name, value)
                session.expunge(result[0])
                session.commit()
                self.respond(200, result[0])
            elif len(result) == 0:
                self.respond(404, None)
                return
            else:
                self.respond(409, "Multiple instances found")
                return
        except KeyError as key_err:
            print("Entity not found")
            traceback.print_exc()
            self.respond(404, str(key_err))
        except Exception as ex:
            print("Unexpected error during PATCH", ex)
            traceback.print_exc()
            self.respond(500, "Unexpected error during PATCH method")
        finally:
            session.close()

    # print("###############START###############")
    # print(data_class.__name__)
    # meta = inspect(data_class)
    # for x in meta.mapped_table.foreign_keys:
    #     print("FOREIGN KEY:", x, str(x.column.table.name))
    # for name, value in data_class.__dict__.items():
    #     if isinstance(value, InstrumentedAttribute):
    #         if isinstance(value, ForeignKeyConstraint):
    #             print("IMAM GA!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    #
    #         print(name, value, value.__class__)
    #         for key, val in value.__dict__.items():
    #             print('\t', key, val)
    #             if key == 'key':
    #                 print(value.__dict__)
    # print("###############END###############")

    print('***{} created! '.format(name))
    return type(name,
                (CorsMixin, APIHandler),
                {
                    "engine": engine,
                    "respond": respond,
                    "get": get,
                    "delete": delete,
                    "post": post,
                    "patch": patch,
                    "CORS_ORIGIN": '*',
                    "CORS_HEADERS": 'Content-Type, Authorization'
                })


def main(conn_string):
    try:
        Base = automap_base()
        engine = create_engine(conn_string, echo=True)
        Base.prepare(engine, reflect=True)
        handlers = {
            x.__name__: HandlerFactory(name="{}Handler".format(x.__name__),
                                       data_class=x,
                                       classes={
                                           klass.__name__: klass for klass in
                                           Base.classes},
                                       engine=engine) for x in Base.classes}
        routes = list()
        for name, handler in handlers.items():
            routes.append(
                (
                    r"/model/{}/(?P<id>[a-zA-Z0-9_-]+)/?$".format(name),
                    handler
                )
            )
            routes.append(
                (
                    r"/model/{}/?$".format(name),
                    handler
                )
            )

        for url, handler in routes:
            print(" Url: {0:50}, Handler: {1:50}".format(url, handler.__name__))
        application = tornado.web.Application(
            handlers=routes,
            autoreload=True
        )
        application.listen(8888)
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        print("Application terminated by keyboard-interrupt")
    except Exception as ex:
        print("Nucleus runtime exception:", ex)
        traceback.print_exc()


if __name__ == '__main__':
    conn_string = 'mysql+pymysql://root:root@127.0.0.1:3306/employees'
    print("Connection string: ", conn_string)
    main(conn_string)
